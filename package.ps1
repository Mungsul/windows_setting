###############################################################################
# System Configuration
###############################################################################
# Set up Chocolatey
Write-Host "Initializing chocolatey"
choco feature enable -n allowGlobalConfirmation
choco feature enable -n allowEmptyChecksums

$Boxstarter.RebootOk=$false # Allow reboots?
$Boxstarter.NoPassword=$false # Is this a machine with no login password?
$Boxstarter.AutoLogin=$true # Save my password securely and auto-login after a reboot
REG ADD "HKLM\Software\Microsoft\Windows NT\CurrentVersion\Winlogon" /v AutoAdminLogon /t REG_SZ /d 1 /f
# Basic setup
Write-Host "Setting execution policy"
Update-ExecutionPolicy Unrestricted
Set-WindowsExplorerOptions -EnableShowProtectedOSFiles -EnableShowFileExtensions -EnableShowHiddenFilesFoldersDrives
Disable-BingSearch
Disable-GameBarTips
Disable-ComputerRestore -Drive ${Env:SystemDrive}
Get-AppxPackage -allusers Microsoft.549981C3F5F10 | Remove-AppxPackage
# Disable UAC
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" /v EnableLUA /t REG_DWORD /d "0" /f 

write-host "Disabling Windows garbage from free VM!"
cmd.exe /c sc config sshd start= disabled
cmd.exe /c sc stop sshd
reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "bginfo" /f 

# Disable Updates
write-host "Disabling Windows Update"
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v NoAutoUpdate /t REG_DWORD /d "1" /f 

# Disable Firewall
Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False

# Kill Windows Defender
write-host "Disabling Windows Defender"
Stop-Service WinDefend
Set-Service WinDefend -StartupType Disabled
Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows Defender" -Name "DisableAntiSpyware" -Value 1
Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows Defender" -Name "DisableRoutinelyTakingAction" -Value 1
New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender" -Name DisableAntiSpyware -Value 1 -PropertyType DWORD -Force

# Disable Action Center
write-host "Disabling Action Center notifications"
reg add "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v HideSCAHealth /t REG_DWORD /d "0x1" /f 

# Set windows Aero theme
write-host "Use Aero theme"
reg add "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\ThemeManager" /v DllName /t REG_EXPAND_SZ /d "%SystemRoot%\resources\themes\Aero\Aero.msstyles" /f
reg add "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\ThemeManager" /v ThemeActive /t REG_SZ /d "1" /f
reg add "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes" /v CurrentTheme /t REG_SZ /d "C:\Windows\resources\Themes\aero.theme" /f

# Set a nice S1 wallpaper : 
write-host "Setting a nice wallpaper"
$web_dl = new-object System.Net.WebClient
$wallpaper_url = "https://gitlab.com/Mungsul/windows_setting/-/raw/main/abstract-1920x1080-dark-wallpaper-preview.jpg"
$wallpaper_file = "C:\Users\Public\Pictures\background.png"
$web_dl.DownloadFile($wallpaper_url, $wallpaper_file)
reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v Wallpaper /t REG_SZ /d "C:\Users\Public\Pictures\background.png" /f
reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v WallpaperStyle /t REG_DWORD /d "0" /f 
reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v StretchWallpaper /t REG_DWORD /d "2" /f 
reg add "HKEY_CURRENT_USER\Control Panel\Colors" /v Background /t REG_SZ /d "0 0 0" /f

###############################################################################
# Utilities, Debugger, Disassembler, Scripting
###############################################################################
choco feature enable -n allowGlobalConfirmation
choco install checksum -y
choco install 7zip.install -y
choco install fakenet -y
choco install procexp -y
choco install procmon -y
choco install autoruns -y
choco install tcpview -y
choco install sysmon -y
choco install hxd -y
choco install pebear -y
choco install pestudio --ignore-checksums
choco install pesieve -y
choco install nxlog -y
choco install x64dbg.portable -y
choco install ollydbg -y
choco install openjdk11 -y
choco install jadx -y
choco install pesieve -y
choco install sublimetext3 -y
choco install die -y
choco install fiddler -y
choco install wireshark -y
choco install javadecompiler-gui -y
choco install reshack -y
choco install dnspy -y
choco install git -y
choco install GoogleChrome -y
choco install hashmyfiles -y
choco install regshot -y
choco install network-miner --ignore-checksums
choco install processhacker -y
choco install dependencywalker -y
choco install python -y
refreshenv
choco install pip -y
python -m pip install --upgrade pip
pip install --upgrade setuptools
pip install pefile
pip install yara-python
pip install oletools
pip install frida-tools
pip install requests
pip install pysocks
pip install pycryptodome
pip install malduck


New-Item "C:\Users\$env:username\Desktop\tools" -ItemType Directory

###############################################################################
# Create Desktop Shortcut
###############################################################################

Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\x64dbg.lnk" -TargetPath "C:\ProgramData\chocolatey\lib\x64dbg.portable\tools\release\x64\x64dbg.exe"
Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\x32dbg.lnk" -TargetPath "C:\ProgramData\chocolatey\lib\x64dbg.portable\tools\release\x32\x32dbg.exe"
Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\OLLYDBG.lnk" -TargetPath "C:\Program Files (x86)\OllyDbg\OLLYDBG.EXE"
Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\HxD.lnk" -TargetPath "C:\Program Files\HxD\HxD.exe"
Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\pe-sieve.lnk" -TargetPath "C:\ProgramData\chocolatey\lib\pesieve\tools\pe-sieve.exe"
Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\pestudio.lnk" -TargetPath "C:\ProgramData\chocolatey\lib\PeStudio\tools\pestudio\pestudio.exe"
Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\proexp.lnk" -TargetPath "C:\ProgramData\chocolatey\lib\procexp\tools\procexp.exe"
Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\Autoruns.lnk" -TargetPath "C:\ProgramData\chocolatey\lib\AutoRuns\tools\Autoruns.exe"
Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\Tcpview.lnk" -TargetPath "C:\ProgramData\chocolatey\lib\TcpView\Tools\Tcpview.exe"
Install-ChocolateyShortcut -ShortcutFilePath "C:\Users\$env:username\Desktop\tools\Wireshark.lnk" -TargetPath "C:\Program Files\Wireshark\Wireshark.exe"


New-Item "C:\Users\$env:username\Desktop\scripts" -ItemType Directory

$web_dl.DownloadFile("https://gitlab.com/Mungsul/windows_setting/-/raw/main/scripts/dllloader.py", "C:\Users\$env:username\Desktop\scripts\dllloader.py")

New-Item "C:\Users\$env:username\Desktop\targets" -ItemType Directory



Write-Host -NoNewline "====== Install Completed ======"
