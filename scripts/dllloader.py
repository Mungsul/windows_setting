from ctypes import cdll
import os
import sys

if len(sys.argv) != 2:
    print(f'[-] {sys.argv[0]} target.dll')
    sys.exit(-1)

cwd = os.getcwd()
dll_path = os.path.join(cwd, sys.argv[1])
input("go")
cdll.LoadLibrary(dll_path)
